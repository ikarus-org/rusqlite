//! `ToSql` and `FromSql` implementation for [`uuid::Uuid`].
use crate::types::{FromSql, FromSqlError, FromSqlResult, ToSql, ToSqlOutput, ValueRef};
use crate::Result;
use uuid::Uuid;

use std::str::FromStr;

/// Serialize `Uuid` to text.
impl ToSql for Uuid {
    fn to_sql(&self) -> Result<ToSqlOutput<'_>> {
        Ok(ToSqlOutput::from(self.to_hyphenated().to_string()))
    }
}

/// Deserialize text to `Uuid`.
impl FromSql for Uuid {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        match value {
            ValueRef::Text(s) => {
                let s = std::str::from_utf8(s).map_err(|e| FromSqlError::Other(Box::new(e)))?;
                Uuid::from_str(s).map_err(|e| FromSqlError::Other(Box::new(e)))
            }
            _ => Err(FromSqlError::InvalidType),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{params, Connection, Error, Result};
    use uuid::{Uuid};
    use std::str::FromStr;

    fn checked_memory_handle() -> Connection {
        let db = Connection::open_in_memory().unwrap();
        db.execute_batch("CREATE TABLE uuids (i INTEGER, v TEXT)")
            .unwrap();
        db
    }

    fn get_uuid(db: &Connection, id: i64) -> Result<Uuid> {
        db.query_row("SELECT v FROM uuids WHERE i = ?", params![id], |r| r.get(0))
    }

    #[test]
    fn test_sql_uuid() {
        let db = &checked_memory_handle();

        let uuid0 = Uuid::from_str("e7ae035e-33ee-4a17-a526-67865fd18e64").unwrap();
        let uuid1 = Uuid::from_str("fb07c734-fbd8-42fd-a1f8-03734b49e13f").unwrap();
        let uuid2 = "e34f2624-1019-467c-9578-05355d2c916f";

        db.execute(
            "INSERT INTO uuids (i, v) VALUES (0, ?), (1, ?), (2, ?), (3, ?)",
            params![uuid0, uuid1, uuid2, "illegal"],
        )
            .unwrap();

        assert_eq!(get_uuid(db, 0).unwrap(), uuid0);

        assert_eq!(get_uuid(db, 1).unwrap(), uuid1);

        let out_uuid2: Uuid = get_uuid(db, 2).unwrap();
        assert_eq!(out_uuid2, Uuid::from_str(uuid2).unwrap());

        // Make sure the conversion error comes through correctly.
        let err = get_uuid(db, 3).unwrap_err();
        match err {
            Error::FromSqlConversionFailure(_, _, e) => {
            }
            e => {
                panic!("Expected conversion failure, got {}", e);
            }
        }
    }
}
